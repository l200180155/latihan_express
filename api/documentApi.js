import express from "express";
import { create, list, find } from "../controller/documentController.js";

const router = express.Router();

router.get('/list', list);
router.post('/create', create);
router.get('/find/:id', find);

export default router