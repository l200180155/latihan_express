const documents = []

class Document {
    constructor(params)
    {
        this.id = params.id;
        this.nama = params.nama;
        this.penulis = params.penulis;
    }

    static list()
    {
        return documents;
    }

    static create(params)
    {
        const document = new this(params)

        documents.push(document)

        return document;
    }

    static find(id)
    {
        const document = documents.find((i) => i.id === Number(id));
        if(!document)
            return null;
        
        return document;
    }
}

export default Document