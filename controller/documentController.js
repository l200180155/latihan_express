import Document from '../resources/document.js'

export const create = (req, res) => {
    const document = Document.create(req.body)

    return res.status(200).json(document)
}

export const list = (req, res) => {
    const documents = Document.list();

    return res.status(200).json(documents)
}

export const find = (req, res) => {
    const document = Document.find(req.params.id);

    return res.status(200).json(document)
}